(function () {
  document.body.style.margin = '0'
  document.body.style.padding = '0'
  document.body.style.width = '100vw'
  document.body.style.height = '100vh'
  document.body.style.overflow = 'hidden'
  var ifr = document.createElement('iframe')
  ifr.style.margin = '0'
  ifr.style.width = '100%'
  ifr.style.height = '100%'
  document.body.appendChild(ifr)

  function loadFrame (url, callback) {
    function eventFunction (evt) {
      ifr.removeEventListener('load', eventFunction)
      callback(ifr.contentDocument, ifr)
    }
    if (url) {
      ifr.src = url
    }
    ifr.addEventListener('load', eventFunction)
  }

  var stub = 1

  function createOne (callback) {
    loadFrame(`/general/workflow/new/do.php?FLOW_ID=3062&f=create&FLOW_TYPE=1&FUNC_ID=2&DATA_RUN_ID=`, function (context) {
      function enterData () {
        let projectInput = context.querySelector('#DATA_15')
        if (!projectInput) {
          setTimeout(enterData, 20)
          return
        }
        projectInput.value = 'Page test'
        context.querySelector('#DATA_2').value = stub
        context.querySelector('#tableDetailChar_18_合同清单审批表明细字段 > tbody > tr.tableheader > td:nth-child(1) > i').dispatchEvent(new MouseEvent('click', { bubbles: true }))
        stub ++

        function enterProduct () {
          var productIdInput = context.querySelector('#合同清单审批表明细字段_2_1')
          if (!productIdInput) {
            setTimeout(enterProduct, 20)
            return
          }
          productIdInput.value = stub
          context.querySelector('#合同清单审批表明细字段_2_2').value = stub.toString(2)
          context.querySelector('#合同清单审批表明细字段_2_3').value = 'SET'
          context.querySelector('#合同清单审批表明细字段_2_4').value = (Math.floor(Math.random() * 100) + 1).toString()
          context.querySelector('#合同清单审批表明细字段_2_5').value = (Math.floor(Math.random() * 500) / 100 + 0.01).toString()
          clickSubmitBtn()
        }

        function clickSubmitBtn () {
          context.querySelector('#turnbutton').dispatchEvent(new MouseEvent('click', { bubbles: true }))
          setTimeout(function () {
            var dialog = context.querySelector('#jsbodyid > div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.my_dialog')
            if (!dialog) {
              setTimeout(clickSubmitBtn, 100)
            } else {
              doSubmit()
            }
          }, 100)
        }

        function doSubmit () {
          var childIframe = context.querySelector('#inputFlowShowDialogIframe')
          if (!childIframe || !childIframe.contentDocument) {
            setTimeout(doSubmit, 20)
            return
          }
          var submitBtn = childIframe.contentDocument.querySelector('#submitButtonId')
          if (!submitBtn) {
            setTimeout(doSubmit, 20)
            return
          }
          submitBtn.dispatchEvent(new MouseEvent('click', { bubbles: true }))
          setTimeout(function () {
            stub ++
            createOne()
          }, 1000)
        }

        enterProduct()
      }
      enterData()
    })
  }

  createOne()

})()
