'use strict';

var PROJECTS = [
  'Test',
  'Page test'
]

;(function () {
  document.body.style.margin = '0'
  document.body.style.padding = '0'
  document.body.style.width = '100vw'
  document.body.style.height = '100vh'
  document.body.style.overflow = 'hidden'
  var uiContainer = document.createElement('div')
  document.body.appendChild(uiContainer)
  // TODO: Warning for 360 browser.
  var projectArgInput = document.createElement('input')
  projectArgInput.setAttribute('type', 'text')
  projectArgInput.setAttribute('placeholder', '项目名称')
  projectArgInput.style.width = '300px'
  uiContainer.appendChild(projectArgInput)

  var tableFieldsMap =
    'ctId    , id      , fi  , ctName      , ctSpec  , exName      , exSpec  , unit, ctPrice , ctSum   , ctAmount, sigAmount, exAmount, itnAdjustAmount, rawPrice, prchAmount, wgAdjustAmount, prchPrice, tProdDone, tDeliver, exRemAmount, remValue, prchRemAmt, prchPdiff, retAmount, retDiffVal, errors'.split(/\s*,\s*/)
  var tableColumnNames =
    '合同编号, 项目编码, 专业, 合同产品名称, 合同规格, 执行产品名称, 执行规格, 单位, 合同单价, 合同金额, 合同量  , 签证量   , 执行量  , 内部调整量     , 成本单价, 采购量    , 施工班组调整量, 采购价格 , 生产完成 , 发货时间, 执行余量   , 余量金额, 采购余量  , 采购价差 , 退货量   , 退货差额  , 错误'.split(/\s*,\s*/)

  var selectors = ['营销', '工程', '财务']
  var selectorsData = [
    // 营销
    '专业,合同编号,项目编码,合同产品名称,合同规格,执行产品名称,执行规格,单位,合同单价,合同金额,合同量,签证量,执行量,内部调整量,执行余量,余量金额,错误'.split(','),
    // 工程
    '专业,合同编号,项目编码,合同产品名称,单位,合同量,执行量,签证量,内部调整量,采购量,施工班组调整量,生产完成,发货时间,采购余量,退货量,退货差额,错误'.split(','),
    // 财务
    '执行产品名称,单位,执行规格,执行量,签证量,内部调整量,采购量,施工班组调整量,成本单价,采购价格,采购余量,错误'.split(',')
  ]
  var selectorInputs = []
  var fieldSelected = []
  function getFieldSelectors () {
    var r = []
    for (var i = 0; i < selectorInputs.length; i ++) {
      r[i] = selectorInputs[i].checked
    }
    return r
  }

  var startBtn = document.createElement('button')

  var checkersUpdate = null
  ;(function () {
    // Create checkboxs for selectors
    for (var i = 0; i < selectors.length; i ++) {
      var ipt = document.createElement('input')
      ipt.type = 'checkbox'
      ipt.id = 'sel' + i
      ipt.checked = false
      uiContainer.appendChild(ipt)
      checkersUpdate = function () {
        var fs = getFieldSelectors()
        var existChecked = false
        for (var i = 0; i < fs.length; i ++) {
          if (fs[i]) {
            existChecked = true
            break
          }
        }

        startBtn.disabled = !existChecked
      }
      checkersUpdate()
      ipt.addEventListener('change', checkersUpdate)
      selectorInputs[i] = ipt
      var lab = document.createElement('label')
      lab.textContent = selectors[i]
      lab.htmlFor = ipt.id
      uiContainer.appendChild(lab)
    }
  })()

  var splitCheck = document.createElement('input')
  splitCheck.type = 'checkbox'
  splitCheck.checked = false
  splitCheck.id = 'splitcheck'
  uiContainer.appendChild(splitCheck)

  var splitCheckLabel = document.createElement('label')
  splitCheckLabel.innerHTML = '以编码<b>及</b>执行规格区分产品'
  splitCheckLabel.htmlFor = splitCheck.id
  splitCheck.addEventListener('change', function (evt) {
    if (splitCheck.checked) {
      for (var i = 0; i <= 1; i ++) {
        var ipt = selectorInputs[i]
        ipt.checked = false
        ipt.disabled = true
        checkersUpdate()
      }
    } else {
      for (var i = 0; i <= 1; i ++) {
        var ipt = selectorInputs[i]
        ipt.disabled = false
      }
    }
  })
  uiContainer.appendChild(splitCheckLabel)

  startBtn.textContent = '开始'
  uiContainer.appendChild(startBtn)

  var projSelect = document.createElement('div')
  projSelect.innerHTML = '或者选择一个项目：'
  ;(function () {
    // Create project quick links
    for (var i = 0; i < PROJECTS.length; i ++) {
      var pro = PROJECTS[i]
      ;(function (pro) {
        var sel = document.createElement('a')
        projSelect.appendChild(sel)
        sel.textContent = pro
        sel.style.display = 'block'
        sel.href = 'javascript:;'
        sel.addEventListener('click', function () {
          projectArgInput.value = pro
          startBtnClick()
        })
      })(pro)
    }
  })()
  uiContainer.appendChild(projSelect)

  var ARG_PROJECT_NAME = null
  var ARG_FIELD_SELECTION = null
  var ARG_SPLIT_DIFFERENT_SPEC = false

  function startBtnClick () {
    // ready
    if (startBtn.disabled) return
    var projName = projectArgInput.value.trim()
    if (projName.length === 0) {
      return
    }
    ARG_PROJECT_NAME = projName
    ARG_FIELD_SELECTION = getFieldSelectors()
    ARG_SPLIT_DIFFERENT_SPEC = splitCheck.checked
    uiContainer.remove()
    // For each field checkbox...
    for (var si = 0; si < ARG_FIELD_SELECTION.length; si ++) {
      // If checked...
      if (ARG_FIELD_SELECTION[si]) {
        var addColumns = selectorsData[si]
        for (var f = 0; f < addColumns.length; f ++) {
          var columnName = addColumns[f]
          var colNameIdx = tableColumnNames.indexOf(columnName)
          if (colNameIdx < 0) {
            console.error('No column named ' + columnName)
            continue
          }
          var propName = tableFieldsMap[colNameIdx]
          if (fieldSelected.indexOf(propName) >= 0) {
            // Avoid repeated stuff
            continue
          }
          fieldSelected.push(propName)
        }
      }
    }
    fieldSelected.sort(function (a, b) {
      var posA = tableFieldsMap.indexOf(a)
      var posB = tableFieldsMap.indexOf(b)
      if (posA - posB < 0) return -1
      else if (posA - posB > 0) return 1
      else return 0
    })
    if (ARG_SPLIT_DIFFERENT_SPEC) {
      procStage(1)
    } else {
      procStage(0)
    }
  }

  var ifr = document.createElement('iframe')
  ifr.style.margin = '0'
  ifr.style.width = '100%'
  ifr.style.height = '50%'
  ifr.style.display = 'inline-block'
  ifr.style.border = 'none'
  document.body.appendChild(ifr)

  startBtn.addEventListener('click', startBtnClick)

  var frameN = 1
  function loadFrame (url, standaloneFrame, callback) {
    // load a page and callback its context
    // standaloneFrame: true for async (no display to user)
    // callback(targetIFrame.contentWindow.document, targetIFrame, releaseCallback)
    if (typeof standaloneFrame === 'function') {
      callback = standaloneFrame
      standaloneFrame = false
    }
    if (standaloneFrame && frameN > 10) {
      setTimeout(function () {
        loadFrame(url, standaloneFrame, callback)
      }, 500)
      return
    }
    var targetIFrame = ifr
    function eventFunction (evt) {
      targetIFrame.removeEventListener('load', eventFunction)
      if (!standaloneFrame) {
        callback(targetIFrame.contentWindow.document, targetIFrame, null)
      } else {
        callback(targetIFrame.contentWindow.document, targetIFrame, function () {
          targetIFrame.remove()
          frameN --
        })
      }
    }
    if (standaloneFrame) {
      targetIFrame = document.createElement('iframe')
      targetIFrame.style.margin = '0'
      targetIFrame.style.width = '20%'
      targetIFrame.style.height = '20%'
      targetIFrame.style.display = 'inline-block'
      targetIFrame.style.border = 'none'
      document.body.appendChild(targetIFrame)
      document.body.style.overflow = 'auto'
      frameN ++
    }
    if (url) {
      targetIFrame.src = url
    }
    targetIFrame.addEventListener('load', eventFunction)
  }

  function doStage (processGroup, processName, processRow, stageDone) {
    loadFrame('/general/workflow/query/?func_id=5', function (context) {
      function select2Input (value, callback, errorCallback) {
        var ipt = context.querySelector('#select2-drop > div > input')
        if (!ipt) {
          errorCallback()
          return
        }
        ipt.value = value
        ipt.dispatchEvent(new Event('input'))
        setTimeout(function () {
          var lis = context.querySelectorAll('#select2-drop li > div')
          var li = null
          for (var i = 0; i < lis.length; i ++) {
            if (lis[i].textContent.trim() === value) {
              li = lis[i]
              break
            }
          }
          if (!li) {
            errorCallback()
            return
          }
          li.dispatchEvent(new MouseEvent('mouseup', { bubbles: true }))
          setTimeout(callback, 20)
        }, 20)
      }
      function inputProcType () {
        var procTypeInput = context.querySelector('#s2id_FLOW_SORT > a')
        procTypeInput.dispatchEvent(new MouseEvent('mousedown'))
        setTimeout(function () {
          select2Input(processGroup, inputProcName, inputProcType)
        }, 20)
      }

      function inputProcName () {
        var procNameInput = context.querySelector('#s2id_FLOW_ID > a')
        procNameInput.dispatchEvent(new MouseEvent('mousedown'))
        setTimeout(function () {
          select2Input(processName, inputProjectName, inputProcName)
        }, 20)
      }

      function inputProjectName () {
        var filterFields = context.querySelectorAll('#FLOW_FORM_SPAN > table > tbody > tr')
        var projNameInput = null
        var projNameFilteringMode = null
        for (var i = 0; i < filterFields.length; i ++) {
          var currentField = filterFields[i]
          var title = currentField.querySelector('.title')
          if (!title || title.innerText.trim() !== '工程名称') continue
          projNameInput = currentField.querySelector('input[type=text]')
          projNameFilteringMode = currentField.querySelector('td:nth-child(2)')
          break
        }
        if (!projNameInput || !projNameFilteringMode) {
          setTimeout(inputProjectName, 20)
          return
        }
        projNameInput.value = ARG_PROJECT_NAME
        projNameInput.dispatchEvent(new Event('input'))
        projNameFilteringMode.querySelector('button').dispatchEvent(new MouseEvent('click', {bubbles: true}))

        function chooseEqual () {
          var equalOption = projNameFilteringMode.querySelector('ul > li:nth-child(1) > a')
          if (!equalOption) {
            setTimeout(chooseEqual, 20)
            return
          }
          equalOption.dispatchEvent(new MouseEvent('click', {bubbles: true}))
          setTimeout(startQuery, 20)
        }
        chooseEqual()
      }

      function startQuery () {
        var btn = context.querySelector('#formsearch > div.lazy_header > button')
        if (!btn) {
          setTimeout(startQuery, 20)
          return
        }
        btn.click()
        loadFrame(null, extractData)
      }

      var rowUrls = []
      var total = 0
      var currentPage = 1
      function extractData (context, frame) {
        var dataRows = []
        var loadingIndicate = context.querySelector('.lazy_table_loading')
        if (loadingIndicate && loadingIndicate.style.display !== 'none') {
          setTimeout(function () {
            extractData(context, frame)
          }, 20)
          return
        }
        var emptyMarker = context.querySelector('div.empty_tip')
        if (emptyMarker && emptyMarker.style.display !== 'none') {
          initProcessRowUrl()
          return
        }
        var tableBottom = context.querySelector('.lazy_table_bottom')
        if (tableBottom.style.display === 'none') {
          setTimeout(function () {
            extractData(context, frame)
          }, 20)
          return
        }
        var totalCount = tableBottom.querySelector('.total-count')
        if (!totalCount) {
          setTimeout(function () {
            extractData(context, frame)
          }, 20)
          return
        }
        if (currentPage === 1) {
          total = parseInt(totalCount.innerText.trim())
        }
        dataRows = context.querySelectorAll('.lazy_table_tr')
        function extractRow (i) {
          if (i >= dataRows.length) {
            if (rowUrls.length < total) {
              var pageInput = context.querySelector('#table > div.lazy_table_bottom > div.pagination_wrap.pagination > span.goto-page-wrap > input')
              currentPage++
              pageInput.dispatchEvent(new Event('focus'))
              setTimeout(function () {
                pageInput.value = currentPage
                setTimeout(function () {
                  pageInput.dispatchEvent(new Event('blur'))
                  setTimeout(function () {
                    extractData(context, frame)
                  }, 20)
                }, 20)
              }, 20)
            } else {
              initProcessRowUrl()
            }
            return
          }
          var dataRow = dataRows[i]
          var td = dataRow.querySelector('td:nth-child(2)')
          if (!td) {
            setTimeout(function () {
              extractRow(i)
            }, 20)
            return
          }
          frame.contentWindow.open = function (url) {
            rowUrls.push(url)
            setTimeout(function () {
              extractRow(i + 1)
            }, 20)
          }
          td.dispatchEvent(new MouseEvent('click', { bubbles: true }))
        }
        extractRow(0)
      }

      var totalAmount = 0
      var stageDoneCalled = false
      var finishAmount = 0
      function initProcessRowUrl () {
        totalAmount = rowUrls.length
        if (totalAmount === 0) {
          stageDoneCalled = true
          stageDone()
          return
        }
        for (var r = 0; r < 9; r ++) {
          processRowUrl()
        }
      }
      function processRowUrl () {
        if (rowUrls.length === 0) {
          return
        }
        var currentUrl = rowUrls.pop()
        loadFrame(currentUrl, true, function (context, frame, frameFree) {
          var contractId = null
          function findMainTableTdByFieldName (field) {
            if (typeof field !== 'string' && !(field instanceof RegExp)) throw new Error('Expected string.')

            var mainTableTds = context.querySelectorAll('td')
            var targetI = null
            for (var i = 0; i < mainTableTds.length; i ++) {
              var text = mainTableTds[i].textContent || mainTableTds[i].innerText
              if (typeof field === 'string' && text.trim() === field) {
                targetI = i
                break
              }
              if (field instanceof RegExp && field.test(text.trim())) {
                if (targetI === null) targetI = i
                else {
                  var ele = mainTableTds[targetI]
                  if ((ele.textContent || ele.innerText).length > text.length) targetI = i
                }
              }
            }
            if (targetI === null || !mainTableTds[targetI] || !mainTableTds[targetI].nextElementSibling) {
              throw new Error('在流程 ' + processName + '中找不到“' + field + '”')
            }
            return mainTableTds[targetI].nextElementSibling
          }

          function extractContractId () {
            try {
              try {
                var mtd = findMainTableTdByFieldName('合同编号')
                if (!mtd) throw new Error()
                var ipt = mtd.querySelector('input, textarea, span > span')
                if (!ipt) {
                  throw new Error()
                }
                contractId = ipt.value ? ipt.value.trim() : (ipt.textContent || ipt.innerText).trim()
              } catch (e) {
                throw new Error('在流程 ' + processName + '中找不到“合同编号”')
              }
            } catch (e) {
              alert(e.message + '。该流程已忽略。相关页面：' + currentUrl)
              contractId = null
              return
            }
          }

          function extractProducts () {
            var productRows = context.querySelectorAll('.detailChar > tbody > tr')
            if (productRows.length === 0) {
              setTimeout(extractProducts, 20)
              return
            }

            var pRow
            var fieldMap = null
            function getField (fieldName) {
              var fieldN = fieldMap[fieldName.trim()]
              if (!fieldN) {
                throw new Error('流程“' + processName + '”中并没有找到“' + fieldName + '”…')
              }
              var td = pRow.querySelector('td:nth-child(' + fieldN + ')')
              var ipt = td.querySelector('input, textarea')
              var value = null
              if (!ipt) {
                value = td.textContent.trim()
              } else {
                value = ipt.value.trim()
              }
              // if (value === '' && fieldName === '项目编码') {
              //   return getField('产品名称')
              // }
              return value
            }
            for (var productI = 0; productI < productRows.length; productI ++) {
              pRow = productRows[productI]
              if (pRow.className.match(/tableheader/)) {
                var tds = pRow.querySelectorAll('td')
                fieldMap = {}
                for (var fI = 0; fI < tds.length; fI ++) {
                  var td = tds[fI]
                  var fName = td.innerText.trim() || td.textContent.trim()
                  if (fName !== '') {
                    fieldMap[fName] = fI + 1
                  }
                }
              } else if (!fieldMap) continue
              else {
                try {
                  // if (getField('项目编码') === 'K02001') {
                  //   console.log(currentUrl)
                  // }
                  processRow(contractId, getField, context, findMainTableTdByFieldName)
                } catch (e) {
                  var prodId = null
                  try {
                    if ((prodId = getField('项目编码'))) {
                      var prod = findOrCreateProduct(contractId, prodId)
                      if (Array.isArray(e)) {
                        for (var eI = 0; eI < e.length; eI ++) {
                          var err = e[eI]
                          prod.errors.push(err.message + ' ( in ' + context.URL + ')')
                        }
                      } else {
                        prod.errors.push(e.message + ' ( in ' + context.URL + ')')
                      }
                    } else {
                      throw new Error()
                    }
                  } catch (_e) {
                    if (prodId !== '') {
                      if (Array.isArray(e)) {
                        if (e[0]) {
                          alert('处理流程 ' + processName + ' ( ' + currentUrl + ' )' + ' 时出错，但无法继续：' + e[0].message)
                        } else {
                          alert('处理流程 ' + processName + ' ( ' + currentUrl + ' )' + ' 时出错，但无法继续。')
                        }
                      } else {
                        alert('处理流程 ' + processName + ' ( ' + currentUrl + ' )' + ' 时出错，但无法继续：' + e.message)
                      }
                    }
                  }
                }
              }
            }
          }

          extractContractId()
          if (typeof contractId === 'string') {
            extractProducts()
            done()
          } else {
            done()
          }
          function done () {
            finishAmount ++
            frameFree()
            if (finishAmount >= totalAmount) {
              if (stageDoneCalled) {
                alert('!')
                throw new Error('!')
              }
              stageDoneCalled = true
              stageDone()
            } else {
              processRowUrl()
            }
          }
        })
      }

      inputProcType()
    })
  }

  var productIdvStats = {}
  function findOrCreateProduct (idPrefix, id, spec) {
    if (typeof idPrefix !== 'string') {
      throw new Error('idPrefix should be string.')
    }
    if (typeof id !== 'string') {
      throw new Error('id should be string.')
    }
    // if (id.length === 0) {
    //   throw new Error('项目编码不能为空')
    // }
    var key = idPrefix + '\n' + id
    if (ARG_SPLIT_DIFFERENT_SPEC) {
      if (!spec) spec = ''
      key += '\n' + spec
    }
    if (productIdvStats[key]) return productIdvStats[key]
    else {
      var prod = {
        ctId: idPrefix,
        id: id,
        fi: null,
        ctName: null,
        ctSpec: null,
        exName: null,
        exSpec: null,
        unit: null,
        ctPrice: null,
        ctSum: 0,
        ctAmount: 0,
        sigAmount: 0,
        exAmount: 0,
        itnAdjustAmount: 0,
        rawPrice: null,
        prchAmount: 0,
        wgAdjustAmount: 0,
        prchPrice: null,
        tProdDone: null,
        tDeliver: null,
        exRemAmount: NaN,
        remValue: NaN,
        prchRemAmt: NaN,
        prchPdiff: NaN,
        retAmount: 0,
        retDiffVal: NaN,
        errors: []
      }
      // Finally calculated fields should be NaN
      // One-time determined fields should be null
      // Accumulated fields should be its initial value, i.e. 0.
      // Don't include id in the beginning.
      productIdvStats[key] = prod
      return prod
    }
  }

  function assertNullOrEqual (a, b, message) {
    if (Number.isFinite(a) && Number.isFinite(b) && Math.abs(a - b) < 0.000001) return Math.round(a * 10000) / 10000
    if (a !== null && a !== b) throw new Error((message || 'Null or equal assertion failed') + ': ' + a + ' != ' + b)
    if (a === null && (typeof b !== 'string' || b.trim() === '')) new Error('内容不能为空。')
    return a
  }
  function parseSafeFloat (a, message) {
    var i = parseFloat(a)
    if (isNaN(i)) throw new Error(a + ': 不是数字')
    if (!isFinite(i)) throw new Error(i + ': floating point number overflow.')
    return i
  }
  function assertNonZero (val, message) {
    if (typeof val === 'string') {
      if (parseInt(val) === 0) throw new Error(message)
    } else if (val === 0) {
      throw new Error(message)
    }
    return val
  }

  var stages = [
    ['销售管理', '合同清单审批表',
      function (idPrefix, getField) {
        var prod = findOrCreateProduct(idPrefix, getField('项目编码'))
        var errs = []

        try {
          var prodName = getField('产品名称')
          if (prod.ctName === null || prod.ctName.trim().length === 0) {
            prod.ctName = prodName
          } else if (prodName.length > 0 && prod.ctName.indexOf(prodName) < 0) {
            prod.ctName += ' / ' + prodName
          }
        } catch (e) {
          errs.push(e)
        }

        try {
          var prodField = getField('专业')
          assertNullOrEqual(prod.fi, prodField, '产品专业冲突')
          prod.fi = prodField
        } catch (e) {
          errs.push(e)
        }

        try {
          var prodSpec = getField('规格')
          if (prodSpec !== '' && prod.ctSpec !== null && prodSpec !== prod.ctSpec) {
            prod.ctSpec = '<已合并多个不同规格的产品>'
          } else if (prod.ctSpec === null && prodSpec !== '') {
            prod.ctSpec = prodSpec
          }
        } catch (e) {
          errs.push(e)
        }

        try {
          var unit = getField('单位')
          assertNullOrEqual(prod.unit, unit, '单位冲突')
          prod.unit = unit
        } catch (e) {
          // errs.push(e)
          // 单位不计冲突
        }

        if (fieldSelected.indexOf('ctPrice') >= 0) {
          try {
            var ctPrice = parseSafeFloat(getField('合同单价'))
            if (prod.ctPrice === null) {
              prod.ctPrice = ctPrice
            } else {
              prod.ctPrice = '<已合并多个单价不同的产品>'
            }
          } catch (e) {
            errs.push(e)
          }
        }

        try {
          var contractSum = parseSafeFloat(getField('金额'))
          prod.ctSum += contractSum
        } catch (e) {
          errs.push(e)
        }

        try {
          var contractAmount = parseSafeFloat(getField('合同量'))
          prod.ctAmount += contractAmount
        } catch (e) {
          errs.push(e)
        }

        if (errs.length > 0) {
          throw errs
        }
      }
    ],
    ['工程管理', '工程量签证单',
      function (idPrefix, getField) {
        var prod = findOrCreateProduct(idPrefix, getField('项目编码'), getField('规格'))
        var errs = []

        try {
          var prodField = getField('专业')
          assertNullOrEqual(prod.fi, prodField, '产品专业冲突')
          prod.fi = prodField
        } catch (e) {
          errs.push(e)
        }

        try {
          var unit = getField('单位')
          assertNullOrEqual(prod.unit, unit, '单位冲突')
          prod.unit = unit
        } catch (e) {
          // errs.push(e)
          // 单位不计冲突
        }

        try {
          var signAmount = parseSafeFloat(getField('签证量'))
          prod.sigAmount += signAmount
        } catch (e) {
          errs.push(e)
        }

        if (errs.length > 0) {
          throw errs
        }
      }
    ],
    ['销售管理', '工程量执行审批表',
      function (idPrefix, getField) {
        var prodId = getField('项目编码')
        var prod = findOrCreateProduct(idPrefix, prodId, getField('规格'))
        var errs = []

        try {
          var prodName = getField('产品名称')
          if (prod.exName === null || prod.exName.trim().length === 0) {
            prod.exName = prodName
          } else if (prodName.length > 0 && prod.exName.indexOf(prodName) < 0) {
            prod.exName += ' / ' + prodName
          }
        } catch (e) {
          errs.push(e)
        }

        try {
          var prodField = getField('专业')
          assertNullOrEqual(prod.fi, prodField, '产品专业冲突')
          prod.fi = prodField
        } catch (e) {
          errs.push(e)
        }

        try {
          var prodSpec = getField('规格')
          if (prodSpec !== '' && prod.exSpec !== null && prodSpec !== prod.exSpec) {
            prod.exSpec = '<已合并多个不同规格的产品>'
          } else if (prod.exSpec === null && prodSpec !== '') {
            prod.exSpec = prodSpec
          }
        } catch (e) {
          errs.push(e)
        }

        try {
          var unit = getField('单位')
          assertNullOrEqual(prod.unit, unit, '单位冲突')
          prod.unit = unit
        } catch (e) {
          // errs.push(e)
          // 单位不计冲突
        }

        try {
          var executeAmount = parseSafeFloat(getField('执行量'))
          prod.exAmount += executeAmount
        } catch (e) {
          errs.push(e)
        }

        if (fieldSelected.indexOf('rawPrice') >= 0) {
          try {
            var rawPrice = parseSafeFloat(getField('成本价格'))
            var err = null
            try {
              assertNullOrEqual(prod.rawPrice, rawPrice, '成本价格不一致')
            } catch (e) {
              err = e
            }
            if (prod.rawPrice === null) {
              prod.rawPrice = rawPrice
            } else {
              prod.rawPrice = Math.max(prod.rawPrice, rawPrice)
            }
            if (err) {
              throw err
            }
          } catch (e) {
            errs.push(e)
          }
        }

        if (errs.length > 0) {
          throw errs
        }
      }
    ],
    ['工程管理', '内部工料调整单',
      function (idPrefix, getField) {
        var prod = findOrCreateProduct(idPrefix, getField('项目编码'), getField('规格'))
        var errs = []

        try {
          var prodField = getField('专业')
          assertNullOrEqual(prod.fi, prodField, '产品专业冲突')
          prod.fi = prodField
        } catch (e) {
          errs.push(e)
        }

        try {
          var unit = getField('单位')
          assertNullOrEqual(prod.unit, unit, '单位冲突')
          prod.unit = unit
        } catch (e) {
          // errs.push(e)
          // 单位不计冲突
        }

        let adjAmountErr = null
        try {
          var internalAdjustAmount = parseSafeFloat(getField('内部调整量（增加）'))
          prod.itnAdjustAmount += internalAdjustAmount
        } catch (e) {
          adjAmountErr = e
        }
        try {
          if (adjAmountErr !== null) {
            adjAmountErr = null
            var internalAdjustAmountNeg = parseSafeFloat(getField('内部调整量（减少）'))
            prod.itnAdjustAmount -= internalAdjustAmountNeg
          }
        } catch (e) {
          adjAmountErr = e
        }

        if (fieldSelected.indexOf('rawPrice') >= 0) {
          try {
            var rawPrice = parseSafeFloat(getField('成本价格'))
            var err = null
            try {
              assertNullOrEqual(prod.rawPrice, rawPrice, '成本价格不一致')
            } catch (e) {
              err = e
            }
            if (prod.rawPrice === null) {
              prod.rawPrice = rawPrice
            } else {
              prod.rawPrice = Math.max(prod.rawPrice, rawPrice)
            }
            if (err) {
              throw err
            }
          } catch (e) {
            errs.push(e)
          }
        }

        if (errs.length > 0) {
          throw errs
        }
      }
    ],
    ['采购管理', '采购审批表',
      function (idPrefix, getField, context, findMainTableTdByFieldName) {
        var prod = findOrCreateProduct(idPrefix, getField('项目编码'), getField('规格'))
        var errs = []

        try {
          var prodField = getField('专业')
          assertNullOrEqual(prod.fi, prodField, '产品专业冲突')
          prod.fi = prodField
        } catch (e) {
          errs.push(e)
        }

        try {
          var unit = getField('单位')
          assertNullOrEqual(prod.unit, unit, '单位冲突')
          prod.unit = unit
        } catch (e) {
          // errs.push(e)
          // 单位不计冲突
        }

        try {
          var purchaseAmount = parseSafeFloat(getField('采购量'))
          prod.prchAmount += purchaseAmount
        } catch (e) {
          errs.push(e)
        }

        if (fieldSelected.indexOf('prchPrice') >= 0) {
          try {
            var prchPrice = parseSafeFloat(getField('采购单价'))
            var err = null
            try {
              assertNullOrEqual(prod.prchPrice, prchPrice, '采购价格不一致')
            } catch (e) {
              err = e
            }
            if (prod.prchPrice === null) {
              prod.prchPrice = prchPrice
            } else {
              prod.prchPrice = Math.max(prod.prchPrice, prchPrice)
            }
            if (err) {
              throw err
            }
          } catch (e) {
            errs.push(e)
          }
        }

        function passTime (tspans, initTime) {
          // Iterate table and get largest timestamp visiable.
          if (!tspans.length) return initTime
          var largestTime = initTime
          for (var i = 0; i < tspans.length; i ++) {
            var tspan = tspans[i]
            var time = tspan.innerText || tspan.textContent
            var date = new Date(time)
            if (isNaN(date.getTime())) continue
            if (!largestTime) largestTime = date
            else if (date.getTime() > largestTime.getTime()) largestTime = date
          }
          return largestTime
        }

        prod.tProdDone = passTime(findMainTableTdByFieldName(/生产完成情况/).querySelectorAll('.counter-sign-div-sign-date-time-span'), prod.tProdDone)
        prod.tDeliver = passTime(findMainTableTdByFieldName(/发货款支付情况/).querySelectorAll('.counter-sign-div-sign-date-time-span'), prod.tDeliver)

        if (errs.length > 0) {
          throw errs
        }
      }
    ],
    ['工程管理', '施工班组调整单',
      function (idPrefix, getField) {
        var prod = findOrCreateProduct(idPrefix, getField('项目编码'), getField('规格'))
        var errs = []

        try {
          var prodField = getField('专业')
          assertNullOrEqual(prod.fi, prodField, '产品专业冲突')
          prod.fi = prodField
        } catch (e) {
          errs.push(e)
        }

        try {
          var unit = getField('单位')
          assertNullOrEqual(prod.unit, unit, '单位冲突')
          prod.unit = unit
        } catch (e) {
          // errs.push(e)
          // 单位不计冲突
        }

        try {
          var workGroupAdjustment = parseSafeFloat(getField('调整量'))
          prod.wgAdjustAmount += workGroupAdjustment
        } catch (e) {
          errs.push(e)
        }

        if (errs.length > 0) {
          throw errs
        }
      }
    ],
    ['工程管理', '退货审批及签收清单表',
      function (idPrefix, getField) {
        var prod = findOrCreateProduct(idPrefix, getField('项目编码'), getField('规格'))
        var errs = []

        try {
          var prodField = getField('专业')
          assertNullOrEqual(prod.fi, prodField, '产品专业冲突')
          prod.fi = prodField
        } catch (e) {
          errs.push(e)
        }

        try {
          var unit = getField('单位')
          assertNullOrEqual(prod.unit, unit, '单位冲突')
          prod.unit = unit
        } catch (e) {
          // errs.push(e)
          // 单位不计冲突
        }

        try {
          var returnAmount = parseSafeFloat(getField('退货量'))
          prod.retAmount += returnAmount
        } catch (e) {
          errs.push(e)
        }

        if (errs.length > 0) {
          throw errs
        }
      }
    ]
  ]

  function wrapTd (columns, haveError) {
    var sBuffer = []
    var currentColumn = null
    for (var i = 0; i < columns.length; i ++) {
      sBuffer.push('<td>')
      if (haveError) sBuffer.push('<font color="#283333">')
      var dummy = document.createElement('div')
      currentColumn = columns[i]
      dummy.textContent = currentColumn
      if (currentColumn === null) dummy.textContent = ''
      else if (currentColumn.getTime && currentColumn.toLocaleDateString) dummy.textContent = currentColumn.toLocaleDateString()
      else if (Array.isArray(currentColumn)) dummy.textContent = currentColumn.join(', ')
      sBuffer.push(dummy.innerHTML)
      if (haveError) sBuffer.push('</font>')
      sBuffer.push('</td>')
    }
    return sBuffer.join('')
  }

  function procStage (i) {
    if (i >= stages.length) {
      // Done, organize and export data
      var stuff = ['<table>']
      var columnNames = []
      for (var fi = 0; fi < fieldSelected.length; fi ++) {
        var fidx = tableFieldsMap.indexOf(fieldSelected[fi])
        if (fidx < 0) columnNames.push(fieldSelected[fi])
        else columnNames.push(tableColumnNames[fidx])
      }
      stuff.push(wrapTd(columnNames))
      for (var productId in productIdvStats) {
        if (!productIdvStats.hasOwnProperty(productId)) continue
        var product = productIdvStats[productId]
        if (product.id.trim() === '') continue
        var haveError = product.errors.length > 0
        // 执行余量 = (小数) 合同量 + (小数) 签证量 - (小数) 执行量 - (小数) 内部调整量
        product.exRemAmount = parseFloat(product.ctAmount) + parseFloat(product.sigAmount) - parseFloat(product.exAmount) - parseFloat(product.itnAdjustAmount)

        // 采购余量 = (小数) 执行量 + (小数) 内部调整量 - (小数) 采购量 - (小数) 施工班组调整量
        product.prchRemAmt = parseFloat(product.exAmount) + parseFloat(product.itnAdjustAmount) - parseFloat(product.prchAmount) - parseFloat(product.wgAdjustAmount)

        // 采购价差 = ( (小数) 成本单价 - (小数) 采购价格 ) * (小数) 采购量
        if (product.rawPrice === null && product.prchPrice === null) {
          product.prchPdiff = '无法计算，因为成本单价和采购价格都未知'
        } else if (product.rawPrice === null) {
          product.prchPdiff = '无法计算，因为成本单价未知'
        } else if (product.prchPrice === null) {
          product.prchPdiff = '无法计算，因为采购价格未知'
        } else {
          product.prchPdiff = (parseFloat(product.rawPrice) - parseFloat(product.prchPrice)) * parseFloat(product.prchAmount)
        }

        // 退货差额 = (小数) 采购余量 + (小数) 退货量
        product.retDiffVal = parseFloat(product.prchRemAmt) + parseFloat(product.retAmount)

        // 余量金额 = (小数) 执行余量 * (小数) 合同单价
        product.remValue = parseFloat(product.exRemAmount) * parseFloat(product.ctPrice)
        if (!haveError) stuff.push('<tr>')
        else stuff.push('<tr bgcolor="#FFCCCC">')
        var cols = []
        for (var coli = 0; coli < fieldSelected.length; coli ++) {
          cols.push(product[fieldSelected[coli]])
        }
        stuff.push(wrapTd(cols, haveError))
        stuff.push('</tr>')
      }
      stuff.push('</table>')
      var bom = '%ef%bb%bf%0a'
      var fileContent = stuff.join('')
      var link = document.createElement('a')
      link.setAttribute('href', 'data:text/csv;charset=utf-8,' + bom + encodeURIComponent(fileContent))
      link.setAttribute("download", ARG_PROJECT_NAME + '-项目汇总表.xls')
      link.textContent = '下载报表！'
      document.body.appendChild(link)
      document.body.appendChild(document.createElement('br'))
      ifr.remove()
      return
    }
    var cStage = stages[i]
    doStage(cStage[0], cStage[1], cStage[2], function () {
      procStage(i + 1)
    })
  }
})()
