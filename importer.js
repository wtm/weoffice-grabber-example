(function () {
  document.body.style.margin = '0'
  document.body.style.padding = '0'
  document.body.style.width = '100vw'
  document.body.style.height = '100vh'
  document.body.style.overflow = 'hidden'
  var topTextarea = document.createElement('textarea')
  topTextarea.style.margin = '0'
  topTextarea.style.width = '100%'
  topTextarea.style.height = '10%'
  topTextarea.style.display = 'block'
  topTextarea.setAttribute('placeholder', '粘贴excel表或者流程链接')
  document.body.appendChild(topTextarea)
  var optionsPanel = document.createElement('div')
  optionsPanel.style.position = 'absolute'
  optionsPanel.style.left = '0'
  optionsPanel.style.right = '0'
  optionsPanel.style.bottom = '90%'
  optionsPanel.style.height = '30px'
  optionsPanel.style.lineHeight = '30px'
  optionsPanel.style.fontSize = '16px'
  optionsPanel.style.backgroundColor = 'rgb(230, 230, 230)'
  document.body.appendChild(optionsPanel)
  var whichTableOption = document.createElement('div')
  whichTableOption.style.display = 'inline-block'
  whichTableOption.style.marginRight = '10px'
  whichTableOption.textContent = '放到第几个表格中？'
  optionsPanel.appendChild(whichTableOption)
  var whichTableInput = document.createElement('input')
  whichTableInput.type = 'number'
  whichTableInput.min = 1
  whichTableInput.max = 1
  whichTableInput.value = 1
  whichTableInput.style.width = '40px'
  whichTableOption.appendChild(whichTableInput)
  var errorDisplay = document.createElement('div')
  errorDisplay.style.display = 'inline-block'
  errorDisplay.style.color = 'red'
  optionsPanel.appendChild(errorDisplay)
  var ifr = document.createElement('iframe')
  ifr.style.margin = '0'
  ifr.style.width = '100%'
  ifr.style.height = '90%'
  ifr.style.display = 'block'
  ifr.style.border = 'none'
  document.body.appendChild(ifr)
  ifr.src = '/general/workflow/new/'
  ifr.addEventListener('load', function () {
    ifr.contentWindow.open = function (url) {
      ifr.contentWindow.location.replace(url)
    }
  })

  setInterval(function () {
    var putContentTo = null
    var chars = ifr.contentWindow.document.querySelectorAll('.detailChar')
    whichTableInput.min = Math.min(chars.length, 1)
    whichTableInput.max = chars.length
    if (chars.length === 0) {
      errorDisplay.textContent = '这里没有表可以贴。'
    } else if (whichTableInput.value > chars.length || whichTableInput.value < 1) {
      errorDisplay.textContent = '这里只有' + chars.length + '个表。'
    } else {
      errorDisplay.textContent = ''
      putContentTo = chars[whichTableInput.value - 1]
    }

    var content = topTextarea.value.trim()
    if (/^https?:\/\/[^\n]+/.test(content)) {
      ifr.contentWindow.location.assign(content)
      topTextarea.value = ''
    } else if (content.length > 0 && putContentTo) {
      var rows = [['']]
      for (var i = 0; i < content.length; i ++) {
        var cChar = content[i]
        var lastRow = rows[rows.length - 1]
        if (/^[\n\r]$/.test(cChar)) {
          if (lastRow.length === 0) {
            continue
          }
          rows.push([''])
        } else if (/^\t$/.test(cChar)) {
          lastRow.push('')
        } else {
          lastRow[lastRow.length - 1] += cChar
        }
      }
      topTextarea.value = ''
      processRows(rows, putContentTo)
    }
  }, 100)

  function processRows (rows, targetTable) {
    var addbtn = targetTable.querySelector('tr.tableheader > td:nth-child(1) > i')
    if (!addbtn) {
      errorDisplay.textContent = '你似乎不能编辑这个流程。'
      return
    }
    if (rows.length === 0) {
      return
    }
    for (var i = 0; i < rows.length; i ++) {
      var currentRow = rows[i]
      addbtn.click()
      var tableRow = targetTable.querySelector('tr:nth-last-child(2)')
      for (var colI = 0; colI < currentRow.length; colI ++) {
        var td = tableRow.querySelector('td:nth-child(' + (3 + colI).toString() + ')')
        if (!td) continue
        var tInput = td.querySelector('input')
        if (!tInput) tInput = td.querySelector('textarea')
        if (!tInput) continue
        tInput.value = currentRow[colI]
        tInput.dispatchEvent(new Event('input'))
      }
    }
  }
})()
