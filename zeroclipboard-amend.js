(function t() {
  document.removeEventListener('readystatechange', t)
  if (document.readyState == 'loading') {
    document.addEventListener('readystatechange', t)
  } else {
    init()
  }
  function init() {
    var btn = document.querySelector('#copyformlink')
    var parent = btn.parentNode
    var oldbtn = btn
    btn = btn.cloneNode(true)
    parent.replaceChild(btn, oldbtn)
    btn.addEventListener('click', function (evt) {
      var tables = document.querySelectorAll('table')
      var strs = []
      for (var i = 0; i < tables.length; i++) {
        var children = tables[i].children
        var tbody = null
        for (var c = 0; c < children.length; c++) {
          var ch = children[c]
          if (ch.tagName.toLowerCase() == 'tbody') {
            tbody = ch
          }
          break
        }
        if (!tbody) {
          continue
        }
        var trs = tables[i].querySelectorAll('tbody > tr')
        var buffer = []
        for (var j = 0; j < trs.length; j++) {
          var tds = trs[j].children
          var str = ''
          for (var k = 0; k < tds.length; k++) {
            var td = tds[k]
            if (td.tagName.toLowerCase() != 'td') continue
            if (k > 0) {
              str += '\t'
            }
            var ipts = td.children
            for (var m = 0; m < ipts.length; m++) {
              var ipt = ipts[m]
              var tag = ipt.tagName.toLowerCase()
              if (tag == 'input' || tag == 'textarea') {
                str += escapeString(ipt.value)
              } else if (tag == 'span') {
                str += escapeString(ipt.innerText)
              }
            }
            if (ipts.length == 0) {
              str += escapeString(td.innerText)
            }
          }
          buffer.push(str)
        }
        strs.push(buffer.join('\n'))
      }
      var nstr = []
      for (var l = 0; l < strs.length; l++) {
        if (strs[l].trim().length > 0) {
          nstr.push(strs[l])
        }
      }
      var str = nstr.join('\n\n')
      putToClipboard(str)
    })
  }

  function putToClipboard(str) {
    var ta = document.createElement('textarea')
    Object.assign(ta.style, {
      position: 'fixed',
      top: 0,
      left: 0,
      width: '1rem',
      height: '1rem',
      padding: 0,
      border: 'none',
      backgroundColor: 'transparent',
      outline: 'none',
      opacity: 0.1
    })
    ta.value = str
    document.body.appendChild(ta)
    ta.select()
    try {
      if (!document.execCommand('copy')) {
        throw new Error('Copy command failed.')
      }
    } catch (e) {
      window.open('data:text/plain;charset=utf-8,' + encodeURIComponent(str))
    }
    ta.remove()
  }

  function escapeString(str) {
    return str.replace(/\n+/g, '\\n').replace(/\t/g, ' ').trim()
  }
})()
