(function () {
  document.body.style.margin = '0'
  document.body.style.padding = '0'
  document.body.style.width = '100vw'
  document.body.style.height = '100vh'
  document.body.style.overflow = 'hidden'
  var ifr = document.createElement('iframe')
  ifr.style.margin = '0'
  ifr.style.width = '100%'
  ifr.style.height = '100%'
  ifr.style.display = 'inline-block'
  ifr.style.border = 'none'
  document.body.appendChild(ifr)

  var frameN = 1
  function loadFrame (url, standaloneFrame, callback) {
    if (typeof standaloneFrame === 'function') {
      callback = standaloneFrame
      standaloneFrame = false
    }
    if (standaloneFrame && frameN > 10) {
      setTimeout(function () {
        loadFrame(url, standaloneFrame, callback)
      }, 500)
      return
    }
    var targetIFrame = ifr
    function eventFunction (evt) {
      targetIFrame.removeEventListener('load', eventFunction)
      if (!standaloneFrame) {
        callback(targetIFrame.contentWindow.document, targetIFrame, null)
      } else {
        callback(targetIFrame.contentWindow.document, targetIFrame, function () {
          targetIFrame.remove()
          frameN --
        })
      }
    }
    if (standaloneFrame) {
      targetIFrame = document.createElement('iframe')
      targetIFrame.style.margin = '0'
      targetIFrame.style.width = '20%'
      targetIFrame.style.height = '20%'
      targetIFrame.style.display = 'inline-block'
      targetIFrame.style.border = 'none'
      document.body.appendChild(targetIFrame)
      document.body.style.overflow = 'auto'
      frameN ++
    }
    if (url) {
      targetIFrame.src = url
    }
    targetIFrame.addEventListener('load', eventFunction)
  }

  var data = []
  var headingIncluded = false

  function startExport (context, frame) {
    var rowUrls = []
    var total = 0
    var currentPage = 1
    function extractData (context, frame) {
      var dataRows = []
      var loadingIndicate = context.querySelector('.lazy_table_loading')
      if (loadingIndicate && loadingIndicate.style.display !== 'none') {
        setTimeout(function () {
          extractData(context, frame)
        }, 20)
        return
      }
      var emptyMarker = context.querySelector('div.empty_tip')
      if (emptyMarker && emptyMarker.style.display !== 'none') {
        initProcessRowUrl()
        return
      }
      var tableBottom = context.querySelector('.lazy_table_bottom')
      if (tableBottom.style.display === 'none') {
        setTimeout(function () {
          extractData(context, frame)
        }, 20)
        return
      }
      var totalCount = tableBottom.querySelector('.total-count')
      if (!totalCount) {
        setTimeout(function () {
          extractData(context, frame)
        }, 20)
        return
      }
      if (currentPage === 1) {
        total = parseInt(totalCount.innerText.trim())
      }
      dataRows = context.querySelectorAll('.lazy_table_tr')
      function extractRow (i) {
        if (i >= dataRows.length) {
          if (rowUrls.length < total) {
            var pageInput = context.querySelector('#table > div.lazy_table_bottom > div.pagination_wrap.pagination > span.goto-page-wrap > input')
            currentPage++
            pageInput.dispatchEvent(new Event('focus'))
            setTimeout(function () {
              pageInput.value = currentPage
              setTimeout(function () {
                pageInput.dispatchEvent(new Event('blur'))
                setTimeout(function () {
                  extractData(context, frame)
                }, 20)
              }, 20)
            }, 20)
          } else {
            initProcessRowUrl()
          }
          return
        }
        var dataRow = dataRows[i]
        var td = dataRow.querySelector('td:nth-child(2)')
        if (!td) {
          setTimeout(function () {
            extractRow(i)
          }, 20)
          return
        }
        frame.contentWindow.open = function (url) {
          rowUrls.push(url)
          setTimeout(function () {
            extractRow(i + 1)
          }, 20)
        }
        td.dispatchEvent(new MouseEvent('click', { bubbles: true }))
      }
      extractRow(0)
    }

    var totalAmount = 0
    var stageDoneCalled = false
    var finishAmount = 0
    function initProcessRowUrl () {
      totalAmount = rowUrls.length
      if (totalAmount === 0) {
        stageDoneCalled = true
        stageDone()
        return
      }
      ifr.style.height = '50%'
      for (var r = 0; r < 9; r ++) {
        processRowUrl()
      }
    }

    function processRowUrl () {
      if (rowUrls.length === 0) {
        return
      }
      var currentUrl = rowUrls.pop()
      loadFrame(currentUrl, true, function (context, frame, frameFree) {
        var tables = context.querySelectorAll('.detailChar')
        for (var tI = 0; tI < tables.length; tI ++) {
          processTable(tables[tI])
        }

        function processTable (table) {
          var trs = table.querySelectorAll('tr')
          var headingTr = table.querySelector('.tableheader')
          for (var rI = 0; rI < trs.length; rI ++) {
            var tr = trs[rI]
            if (tr == headingTr) {
              if (headingIncluded) continue
              headingIncluded = true
            }
            var row = []
            var tds = tr.querySelectorAll('td')
            for (var dI = 0; dI < tds.length; dI ++) {
              var td = tds[dI]
              var text = td.innerText
              var ipt = td.querySelector('input, textarea')
              if (ipt) {
                text += ipt.value
              }
              row.push(text.trim())
            }
            data.push(row)
          }
        }

        finishAmount ++
        frameFree()
        if (finishAmount >= totalAmount) {
          if (stageDoneCalled) {
            alert('!')
            throw new Error('!')
          }
          stageDoneCalled = true
          stageDone()
        } else {
          processRowUrl()
        }
      })
    }

    extractData(context, frame)
  }

  loadFrame('/general/workflow/query/', function (context) {
    loadFrame(null, startExport)
  })

  function wrapTd (columns) {
    var sBuffer = []
    for (var i = 0; i < columns.length; i ++) {
      sBuffer.push('<td>')
      var dummy = document.createElement('div')
      dummy.innerText = columns[i]
      sBuffer.push(dummy.innerHTML)
      sBuffer.push('</td>')
    }
    return sBuffer.join('')
  }

  function stageDone () {
    console.log('DONE!')
    var stuff = ['<table>']
    for (var i = 0; i < data.length; i ++) {
      stuff.push('<tr>')
      stuff.push(wrapTd(data[i]))
      stuff.push('</tr>')
    }
    stuff.push('</table>')
    var bom = '%ef%bb%bf%0a'
    var fileContent = stuff.join('')
    var link = document.createElement('a')
    link.setAttribute('href', 'data:text/csv;charset=utf-8,' + bom + encodeURIComponent(fileContent))
    link.setAttribute("download", 'data.xls')
    link.innerText = 'Download excel file.'
    document.body.appendChild(link)
    document.body.appendChild(document.createElement('br'))
    ifr.remove()
  }
})()
