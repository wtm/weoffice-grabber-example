(function () {
  document.body.style.margin = '0'
  document.body.style.padding = '0'
  document.body.style.width = '100vw'
  document.body.style.height = '100vh'
  document.body.style.overflow = 'hidden'
  var ifr = document.createElement('iframe')
  ifr.style.margin = '0'
  ifr.style.width = '100%'
  ifr.style.height = '100%'
  ifr.style.display = 'block'
  ifr.style.border = 'none'
  document.body.appendChild(ifr)
  ifr.src = '/general/workflow/query/'
  ifr.addEventListener('load', function () {
    ifr.contentWindow.open = function (url) {
      ifr.contentWindow.location.replace(url)
    }
  })

  function removeSelector (selector) {
    var context = ifr.contentWindow.document
    var ele = context.querySelectorAll(selector)
    for (var i = 0; i < ele.length; i ++) {
      ele[i].remove()
    }
  }

  setInterval(function () {
    var context = ifr.contentWindow.document
    var flowViewMarker = context.querySelector('#runnameform > div:nth-child(2) > span')
    if (flowViewMarker && flowViewMarker.innerText.trim() === '流程标题') {
      var inputs = context.querySelectorAll('textarea, input')
      for (var i = 0; i < inputs.length; i ++) {
        var ipt = inputs[i]
        var textNode = document.createTextNode(ipt.value)
        ipt.parentNode.replaceChild(textNode, ipt)
      }
      removeSelector('.lazy_header')
      removeSelector('#flowNoSpan_20')
      removeSelector('.detailChar tr > td:nth-child(1) > i')
    }
  }, 100)
})()
